﻿using System;

namespace Exercice_3
{
    class Program
    {
        static void Main(string[] args)
        {
            string saisie;
            int age;

            Console.WriteLine("Saisir un age");
            saisie = Console.ReadLine();

            try
            {
                age = int.Parse(saisie);

                if (age > 0 && age <120)
                {
                    if (age >= 18)
                    {
                        Console.WriteLine("Vous êtes majeur !");
                    }
                    else
                    {
                        Console.WriteLine("Vous êtes mineur ! ");
                    }
                }
                else
                {
                    Console.WriteLine("L'âge saisi est incorrect !");
                }
            }catch(Exception e)
            {
                Console.WriteLine(e.Message);
                Console.ReadKey();
                return;
            }

            Console.ReadKey();
        }
    }
}