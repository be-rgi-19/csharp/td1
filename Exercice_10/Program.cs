﻿using System;

namespace Exercice_10
{
    class Program
    {
        static void Main(string[] args)
        {
            int nb;

            try
            {
                Console.WriteLine("Saisir un nombre");
                nb = int.Parse(Console.ReadLine());
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Console.ReadKey();
                return;
            }

            for (int i = 1; i < 11; i++)
            {
                Console.WriteLine("{0}x{1}={2}", nb, i, nb * i);
            }
            Console.ReadLine();
        }

    }
}
