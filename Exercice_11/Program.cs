﻿using System;

namespace Exercice_11
{
    class Program
    {
        static void Main(string[] args)
        {
            int s = 0;
            for (int i = 1; i < 101; i++)
            {
                s = s + (i * i);
            }

            Console.WriteLine("S vaut : {0}", s);
            Console.ReadKey();
        }
    }
}
