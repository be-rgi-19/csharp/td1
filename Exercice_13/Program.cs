﻿using System;

namespace Exercice_13
{
    class Program
    {
        static void Main(string[] args)
        {
            double n, total = 0;
            Boolean stop = true;

            do
            {
                try { 
                    Console.WriteLine("Saisir un nombre");
                    n = double.Parse(Console.ReadLine());
                    if (n == 0)
                        stop = false;
                    total = Math.Round(total + n,1);
                
                } catch (Exception e)
                {
                    Console.WriteLine("Saisie incorrecte");
                }
            } while (stop);

            Console.WriteLine("La somme vaut {0}", total);
            Console.ReadKey();
        }
    }
}
