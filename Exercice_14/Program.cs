﻿using System;

namespace Exercice_14
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] T = new int[100];
            int maxvalue = 0, cellmaxvalue = 0;
            Random rnd = new Random();
            int cell = rnd.Next(0, 99);
            T[cell] = 50;
            
            for (int i = 0; i < T.Length; i++)
            {
                if (T[i] > maxvalue) { 
                    maxvalue = T[i];
                    cellmaxvalue = i + 1;
                }
            }
            Console.WriteLine("la cellule numéro {0} contient la plus grande valeur : {1}", cellmaxvalue, maxvalue);
            Console.ReadKey();
        }
    }
}
