﻿using System;

namespace Exercice_15
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            int debut = 1, fin = 100;
            int uservalue;

            do
            {
                var value = (debut + fin) / 2;
                Console.WriteLine("Est-ce que le nombre est égal à {0} ?", value);
                uservalue = int.Parse(Console.ReadLine());
                if (uservalue == 1)
                    fin = value;
                else if (uservalue == -1)
                    debut = value;
                else if (uservalue == 0)
                    Console.WriteLine("Gagné !");

            } while (uservalue != 0);

            Console.ReadKey();
        }
    }
}
