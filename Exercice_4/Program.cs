﻿using System;

namespace Exercice_4
{
    class Program
    {
        static void Main(string[] args)
        {
            int h, m;

            try
            {

                Console.WriteLine("Saisir le nombre d'heures");
                h = int.Parse(Console.ReadLine());
                Console.WriteLine("Saisit le nombre de minutes");
                m = int.Parse(Console.ReadLine());

            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Console.ReadKey();
                return;
            }

            m++;

            if (m == 60)
            {
                m = 0;
                h++;
                if (h == 24)
                {
                    h = 0;
                }
            }

            

            Console.WriteLine("Dans une minute, il sera {0}:{1}", h, m);
            Console.ReadKey();
        }
    }
}
