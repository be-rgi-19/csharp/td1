﻿using System;

namespace Exercice_5
{
    class Program
    {
        static void Main(string[] args)
        {
            int nbCopies;
            float prixTotal;

            try
            {
                Console.WriteLine("Saisir le nombre de copies");
                nbCopies = int.Parse(Console.ReadLine());
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Console.ReadKey(); 
                return;
            }

            if (nbCopies == 10)
            {
                prixTotal = nbCopies * 0.1F;
            }
            else if (nbCopies > 10 && nbCopies <= 30)
            {
                prixTotal = 1 + (nbCopies - 10) * 0.09F;
            }
            else
            {
                prixTotal = 1 + 1.8F + (nbCopies - 30) * 0.08F;
            }

            Console.WriteLine("Le coût des copies est de : {0} €", prixTotal);
            Console.ReadKey();
        }
    }
}
