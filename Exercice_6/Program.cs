﻿using System;

namespace Exercice_6
{
    class Program
    {
        static void Main(string[] args)
        {
            double a, b, c, aire, s;

            Console.WriteLine("Saisir la longueur des 3 côtés");
            try
            {
                a = double.Parse(Console.ReadLine());
                b = double.Parse(Console.ReadLine());
                c = double.Parse(Console.ReadLine());
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Console.ReadKey();
                return;
            }
            //Vérification si le triangle est valide
            if (a+b >= c )
            {
                //Calcul de la demi-somme s 
                s = (a + b + c) / 2;
                aire = Math.Sqrt(s * (s - a) * (s - b) * (s - c));
                Console.WriteLine("Aire : {0}", aire);
            }
            else
            {
                Console.WriteLine("Longueurs invalides");
            }

            Console.ReadKey();
        }
    }
}
