﻿using System;

namespace Exercice_7
{
    class Program
    {
        static void Main(string[] args)
        {
            int n;

            Console.WriteLine("Saisir un nombre");
            try
            {
                n = int.Parse(Console.ReadLine());
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Console.ReadKey();
                return;
            }

            for (int i = 0; i < n; i++)
            {
                Console.Write("*");
            }

            Console.ReadKey();
        }
    }
}
