﻿using System;

namespace Exercice_8
{
    class Program
    {
        static void Main(string[] args)
        {
            for (int nb = 1; nb <= 6; nb++)
            {
                Console.WriteLine("{0} baguette(s) counte(nt) {1} euros", nb, nb * 0.8);
            }
            Console.ReadKey();
        }
    }
}
