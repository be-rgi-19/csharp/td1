﻿using System;

namespace Exercice_9
{
    class Program
    {
        static void Main(string[] args)
        {
            float nbHab = 87646;
            int nbAnnees = 0;

            do
            {
                nbHab = nbHab + (nbHab * 0.89F / 100);
                nbAnnees++;
            } while (nbHab < 100000);

            Console.WriteLine("il faudra {0} années, pour atteindre 100 000 habitants.", nbAnnees);
            Console.ReadKey();
        }
    }
}
