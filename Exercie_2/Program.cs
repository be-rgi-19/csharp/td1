﻿using System;

namespace Exercie_2
{
    class Program
    {
        static void Main(string[] args)
        {
            string prenom;

            Console.WriteLine("Saisir un prénom");
            prenom = Console.ReadLine();

            Console.WriteLine("Bonjour {0} !", prenom);
            Console.ReadKey();
        }
    }
}
